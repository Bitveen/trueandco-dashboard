import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Log in';

    this.state = {
      email: '',
      password: ''
    };

    this.handleLogin = this.handleLogin.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }
  componentDidMount() {
    document.title = this.title;
  }
  componentWillMount() {
    if (this.props.authenticated) {
      this.props.history.push('/');
    }
  }
  handleLogin(event) {
    event.preventDefault();
    this.props.onLogin({
      email: this.state.email,
      password: this.state.password
    });
  }
  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    });
  }
  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }
  render() {
    return (
      <div className="login">
        <form className="login__form" onSubmit={this.handleLogin}>
          <div className="login__body">
            <div className="login__logo" />
            <div className="login__form-group">
              <div>
                <label className="login__label" htmlFor="email">Email</label>
              </div>
              <div>
                <input
                  type="text"
                  id="email"
                  value={this.state.email}
                  onInput={this.handleEmailChange}
                  placeholder="Email"
                  className="login__input" />
              </div>
            </div>
            <div className="login__form-group">
              <div>
                <label className="login__label" htmlFor="password">Password</label>
              </div>
              <div>
                <input
                  type="password"
                  id="password"
                  value={this.state.password}
                  onInput={this.handlePasswordChange}
                  placeholder="Password"
                  className="login__input" />
              </div>
            </div>
            <button className="login__button">
              <i className="fa fa-sign-in" />
              <span className="login__button-label">Log in</span>
            </button>
          </div>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired
};

export default withRouter(Login);
