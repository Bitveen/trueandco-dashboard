import axios from 'axios';

export const request = axios.create({
  baseURL: 'http://dashboard.true.dev:4001/api/',
  timeout: 3000
});
