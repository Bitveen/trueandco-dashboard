import { request } from './http';
import Promise from 'bluebird';

export const getTotalCustomers = () => {
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: 'customers',
      params: { total: 1 }
    }).then((response) => {
      resolve(response.data);
    }, (err) => {
      reject(err);
    });
  });
};

export const getTotalCustomersPerPeriod = (startDate, endDate) => {
  return new Promise((resolve, reject) => {
    let period = startDate;
    if (endDate) {
      period += `:${endDate}`;
    }
    request({
      method: 'GET',
      url: 'customers',
      params: { total: 1, period }
    }).then((response) => {
      resolve(response.data);
    }, (err) => {
      reject(err);
    })
  });
};

export const getCustomersSourcesPerPeriod = (startDate, endDate) => {
  return new Promise((resolve, reject) => {
    let period = startDate;
    if (endDate) {
      period += `:${endDate}`;
    }
    request({
      method: 'GET',
      url: 'customers',
      params: { sources: 1, period }
    }).then((response) => {
      resolve(response.data);
    }, (err) => {
      reject(err);
    })
  });
};
