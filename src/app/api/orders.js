import { request } from './http';
import Promise from 'bluebird';

export const getOrdersPerPeriod = (startDate, endDate, type) => {
  return new Promise((resolve, reject) => {
    let period = startDate;
    if (endDate) {
      period += `:${endDate}`;
    }
    request({
      method: 'GET',
      url: 'orders',
      params: { type, period }
    }).then((response) => {
      console.log(response.data);
      resolve(response.data);
    }, (err) => {
      reject(err);
    });
  });
};

export const getOrdersSourcesPerPeriod = (startDate, endDate) => {
  return new Promise((resolve, reject) => {
    let period = startDate;
    if (endDate) {
      period += `:${endDate}`;
    }
    request({
      method: 'GET',
      url: 'orders',
      params: { source: 1, period }
    });
  });
};

export const getAverageOrderValuesPerPeriod = (startDate, endDate, type) => {
  return new Promise((resolve, reject) => {
    let period = startDate;
    if (endDate) {
      period += `:${endDate}`;
    }
    request({
      method: 'GET',
      url: 'orders',
      params: { average: 1, type, period }
    }).then((response) => {
      resolve(response.data);
    }, (err) => {
      reject(err);
    });
  });
};
