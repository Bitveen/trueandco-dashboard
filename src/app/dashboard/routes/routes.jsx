import React from 'react';

import Orders from '../components/pages/orders/Orders';
import Customers from '../components/pages/customers/Customers';
import Widgets from '../components/pages/widgets/Widgets';
import Conversions from '../components/pages/conversions/Conversions';
import Help from '../components/pages/help/Help';
import Traffic from '../components/pages/traffic/Traffic';
import Quiz from '../components/pages/quiz/Quiz';
import EnteredCheckout from '../components/pages/entered-checkout/EnteredCheckout';
import AddToCart from '../components/pages/add-to-cart/AddToCart';
import Settings from '../components/pages/settings/Settings';
import NewRelic from '../components/pages/new-relic/NewRelic';

export default [
  {
    path: '/',
    component: Widgets,
    exact: true
  },
  {
    path: '/orders',
    component: Orders,
    exact: true
  },
  {
    path: '/customers',
    component: Customers,
    exact: true
  },
  {
    path: '/conversions',
    component: Conversions,
    exact: true
  },
  {
    path: '/help',
    component: Help,
    exact: true
  },
  {
    path: '/traffic',
    component: Traffic,
    exact: true
  },
  {
    path: '/quiz',
    component: Quiz,
    exact: true
  },
  {
    path: '/entered-checkout',
    component: EnteredCheckout,
    exact: true
  },
  {
    path: '/add-to-cart',
    component: AddToCart,
    exact: true
  },
  {
    path: '/settings',
    component: Settings
  },
  {
    path: '/new-relic',
    component: NewRelic,
    exact: true
  }
];


