import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Sidebar from './components/common/Sidebar';
import Topbar from './components/common/Topbar';
import NotFound from './components/pages/not-found/NotFound';
import ScrollToTop from './components/common/ScrollToTop';

import routes from './routes/routes';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    if (!this.props.authenticated) {
      this.props.history.push('/login');
    }
  }
  render() {
    return (
      <ScrollToTop>
        <div className="wrapper">
          <Sidebar />
          <section className="content">
            <Topbar
              authenticated={this.props.authenticated}
              onLogout={this.props.onLogout} />
            <div className="content-body">
              <Switch>
                {routes.map((route, index) => {
                  return <Route key={index} {...route} />
                })}
                <Route component={NotFound} />
              </Switch>
            </div>
          </section>
        </div>
      </ScrollToTop>
    );
  }
}

Dashboard.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired
  }).isRequired,
  onLogout: PropTypes.func.isRequired
};

export default withRouter(Dashboard);
