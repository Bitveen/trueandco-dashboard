import React from 'react';
import PropTypes from 'prop-types';

const StatsHeader = ({ title }) => {
  return (
    <div className="stats__header">
      <h3 className="stats__header-text">{ title }</h3>
    </div>
  );
};

StatsHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default StatsHeader;
