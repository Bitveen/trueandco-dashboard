import React from 'react';
import { Bar } from 'react-chartjs-2';
import chartWithDefaults from '../../utils/charts/chartWithDefaults';

const StatsBarChart = (props) => {
  return (
    <div className="stats__chart">
      <Bar {...props} legend={{ display: false }}/>
    </div>
  );
};

export default chartWithDefaults(StatsBarChart);
