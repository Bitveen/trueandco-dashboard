import React from 'react';
import { Line } from 'react-chartjs-2';
import chartWithDefaults from '../../utils/charts/chartWithDefaults';

const StatsLineChart = (props) => {
  return (
    <div className="stats__chart">
      <Line {...props} />
    </div>
  );
};

export default chartWithDefaults(StatsLineChart);
