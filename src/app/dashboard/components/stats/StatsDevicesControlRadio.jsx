import React from 'react';
import PropTypes from 'prop-types';
import { RadioGroup, Radio } from 'react-radio-group';

const StatsDevicesControlRadio = ({ deviceType, onDeviceTypeChange }) => {
  return (
    <div className="stats__controls-radio">
      <RadioGroup name="deviceType" selectedValue={deviceType} onChange={onDeviceTypeChange}>
        <label className="stats__radio-label">
          <Radio value="desktop" /> Desktop
        </label>
        <label className="stats__radio-label">
          <Radio value="mobile" /> Mobile
        </label>
      </RadioGroup>
    </div>
  );
};

StatsDevicesControlRadio.propTypes = {
  deviceType: PropTypes.string.isRequired,
  onDeviceTypeChange: PropTypes.func.isRequired
};

export default StatsDevicesControlRadio;
