import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

const options = [
  {
    value: 'daily',
    label: 'Daily'
  },
  {
    value: 'monthly',
    label: 'Monthly'
  }
];

const StatsRangeTypeSelect = ({ rangeType, onChange }) => {
  return (
    <div className="stats__select">
      <Select
        value={rangeType}
        clearable={false}
        placeholder="Select range"
        searchable={false}
        onChange={onChange}
        options={options} />
    </div>
  );
};

StatsRangeTypeSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  rangeType: PropTypes.string.isRequired
};

export default StatsRangeTypeSelect;
