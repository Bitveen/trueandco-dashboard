import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';

const StatsDateRange = (props) => {
  return (
    <div className={'stats__date-range ' + (props.right ? 'stats__date-range_right' : '')}>
      <div className="stats__date-range-item">
        <div className="stats__date-range-label">
          <label>From</label>
        </div>
        <div className="stats__date-range-input-block">
          <DatePicker
            selectsStart
            popperPlacement="bottom-center"
            popperModifiers={{
              offset: {
                enabled: true,
                offset: '-12px, 0px'
              }
            }}
            selected={props.startDate}
            startDate={props.startDate}
            endDate={props.endDate}
            onChange={props.onStartDateChange}
            className="stats__date-range-input" />
        </div>
      </div>
      <div className="stats__date-range-item">
        <div className="stats__date-range-label">
          <label>To</label>
        </div>
        <div className="stats__date-range-input-block">
          <DatePicker
            selected={props.endDate}
            selectsEnd
            popperPlacement="bottom-center"
            popperModifiers={{
              offset: {
                enabled: true,
                offset: '-12px, 0px'
              }
            }}
            startDate={props.startDate}
            endDate={props.endDate}
            onChange={props.onEndDateChange}
            className="stats__date-range-input" />
        </div>
      </div>
    </div>
  );
};

StatsDateRange.propTypes = {
  right: PropTypes.bool,
  startDate: PropTypes.object.isRequired,
  endDate: PropTypes.object.isRequired,
  onStartDateChange: PropTypes.func.isRequired,
  onEndDateChange: PropTypes.func.isRequired
};

export default StatsDateRange;
