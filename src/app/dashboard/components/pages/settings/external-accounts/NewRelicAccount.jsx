import React from 'react';

const NewRelicAccount = () => {
  return (
    <div className="settings-block__account">
      <h3 className="settings-block__account-header">New Relic</h3>
      <form>
        <div className="settings-block__form-group">
          <div className="settings-block__form-label">
            <label className="settings-block__label" htmlFor="account">Account</label>
          </div>
          <div className="settings-block__form-input">
            <input type="text" className="settings-block__input" id="account" />
          </div>
        </div>
        <div className="settings-block__form-group">
          <div className="settings-block__form-label">
            <label className="settings-block__label" htmlFor="token">Token</label>
          </div>
          <div className="settings-block__form-input">
            <input type="text" className="settings-block__input" id="token" />
          </div>
        </div>
        <button className="settings-block__button">Save settings</button>
      </form>
    </div>
  );
};

export default NewRelicAccount;
