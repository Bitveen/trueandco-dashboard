import React from 'react';

import SettingsHeader from '../SettingsHeader';
import NewRelicAccount from './NewRelicAccount';

export default class SettingsExternalAccounts extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="settings">
        <div className="settings-block">
          <SettingsHeader title="External accounts" />
          <NewRelicAccount />
        </div>
      </div>
    );
  }
}
