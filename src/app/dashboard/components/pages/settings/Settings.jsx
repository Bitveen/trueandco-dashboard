import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import PageHeader from '../../common/PageHeader';
import SettingsTabs from './SettingsTabs';
import SettingsDirectories from './directories/SettingsDirectories';
import SettingsUsers from './users/SettingsUsers';
import SettingsExternalAccounts from './external-accounts/SettingsExternalAccounts';

export default class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Settings';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div>
        <PageHeader title={this.title} />
        <SettingsTabs />
        <Switch>
          <Route path="/settings" exact render={() => (
            <Redirect to="/settings/directories" />
          )}/>
          <Route path="/settings/directories" component={SettingsDirectories} />
          <Route path="/settings/users" component={SettingsUsers} />
          <Route path="/settings/external-accounts" component={SettingsExternalAccounts} />
        </Switch>
      </div>
    );
  }
}
