import React from 'react';

import TrafficSources from './TrafficSources';
import CustomersChannels from './CustomersChannels';

export default class SettingsDirectories extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="settings">
        <TrafficSources />
        <CustomersChannels />
      </div>
    );
  }
}
