import React from 'react';

const CustomersChannels = () => {
  return (
    <div className="settings-block">
      <div className="settings-block__header">
        Customers channels
      </div>
      <table className="settings-block__table">
        <thead>
        <tr>
          <th>Channel</th>
          <th colSpan="2">Modify</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Digital</td>
          <td className="settings-block__table-button">Edit</td>
          <td className="settings-block__table-button">Delete</td>
        </tr>
        <tr>
          <td>Video</td>
          <td className="settings-block__table-button">Edit</td>
          <td className="settings-block__table-button">Delete</td>
        </tr>
        </tbody>
      </table>
      <button className="settings-block__button">Add new customer channel</button>
    </div>
  );
};

export default CustomersChannels;
