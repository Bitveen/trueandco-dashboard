import React from 'react';

const TrafficSources = () => {
  return (
    <div className="settings-block">
      <div className="settings-block__header">
        Traffic sources
      </div>
      <table className="settings-block__table">
        <thead>
        <tr>
          <th>Traffic source</th>
          <th colSpan="2">Modify</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Facebook AD</td>
          <td className="settings-block__table-button">Edit</td>
          <td className="settings-block__table-button">Delete</td>
        </tr>
        <tr>
          <td>Google Organic</td>
          <td className="settings-block__table-button">Edit</td>
          <td className="settings-block__table-button">Delete</td>
        </tr>
        </tbody>
      </table>
      <button className="settings-block__button">
        Add new traffic source
      </button>
    </div>
  );
};

export default TrafficSources;
