import React from 'react';
import { NavLink } from 'react-router-dom';

const SettingsTabs = () => {
  return (
    <div className="tabs">
      <NavLink
        activeClassName="tabs__tab_active"
        to="/settings/directories"
        className="tabs__tab">Directories</NavLink>
      <NavLink
        activeClassName="tabs__tab_active"
        to="/settings/users"
        className="tabs__tab">Users</NavLink>
      <NavLink
        activeClassName="tabs__tab_active"
        to="/settings/external-accounts"
        className="tabs__tab">External accounts</NavLink>
    </div>
  );
};

export default SettingsTabs;
