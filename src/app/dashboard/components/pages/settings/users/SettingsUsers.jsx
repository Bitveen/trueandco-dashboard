import React from 'react';

import SettingsHeader from '../SettingsHeader';

export default class SettingsUsers extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="settings">
        <div className="settings-block">
          <SettingsHeader title="Users" />
          <table className="settings-block__table">
            <thead>
              <tr>
                <th>User name</th>
                <th>E-Mail</th>
                <th>Phone</th>
                <th colSpan="2">Modify</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alex</td>
                <td>alex@gmail.com</td>
                <td>12345</td>
                <td className="settings-block__table-button">Edit</td>
                <td className="settings-block__table-button">Delete</td>
            </tr>
            </tbody>
          </table>
          <button className="settings-block__button">Add new user</button>
        </div>
      </div>
    );
  }
}
