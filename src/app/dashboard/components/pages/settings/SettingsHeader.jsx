import React from 'react';
import PropTypes from 'prop-types';

const SettingsHeader = ({ title }) => {
  return (
    <div className="settings-block__header">{title}</div>
  );
};

SettingsHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default SettingsHeader;
