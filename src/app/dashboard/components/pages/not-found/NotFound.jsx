import React from 'react';

import PageHeader from '../../common/PageHeader';

export default class NotFound extends React.Component {
  constructor(props) {
    super(props);

    this.title = '404. Not Found';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div>
        <PageHeader title={this.title} />
      </div>
    );
  }
}
