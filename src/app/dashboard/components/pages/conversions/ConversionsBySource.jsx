import React from 'react';

import StatsHeader from '../../stats/StatsHeader';
import StatsBarChart from '../../stats/StatsBarChart';

export default class ConversionsBySource extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        labels: ['Facebook AD', 'Google Organic', 'Hareasale Referral'],
        datasets: [
          {
            borderColor: '#f56b42',
            backgroundColor: '#f56b42',
            data: [100, 80, 190]
          }
        ]
      }
    };
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Conversion by source" />
        <StatsBarChart data={this.state.data} />
        <table className="stats__table">
          <thead>
          <tr>
            <th>Source</th>
            <th>Visitors</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Facebook ad</td>
            <td>100</td>
          </tr>
          <tr>
            <td>Google organic</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Hareasale referral</td>
            <td>190</td>
          </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
