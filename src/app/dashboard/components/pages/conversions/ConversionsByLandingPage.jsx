import React from 'react';
import moment from 'moment';

import StatsHeader from '../../stats/StatsHeader';
import StatsDevicesControlRadio from '../../stats/StatsDevicesControlRadio';
import StatsBarChart from '../../stats/StatsBarChart';

export default class ConversionsByLandingPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        labels: ['/', '/quiz', '/order'],
        datasets: [
          {
            borderColor: '#f56b42',
            backgroundColor: '#f56b42',
            data: [80, 70, 140]
          }
        ]
      },
      deviceType: 'desktop'
    };

    this.handleDeviceTypeChange = this.handleDeviceTypeChange.bind(this);
  }
  handleDeviceTypeChange(deviceType) {
    this.setState({ deviceType });
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Conversion by landing page" />
        <div className="stats__controls">
          <StatsDevicesControlRadio
            onDeviceTypeChange={this.handleDeviceTypeChange}
            deviceType={this.state.deviceType} />
        </div>
        <StatsBarChart data={this.state.data} />
        <table className="stats__table">
          <thead>
          <tr>
            <th>Landing page</th>
            <th>Visitors</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>/</td>
            <td>80</td>
          </tr>
          <tr>
            <td>/quiz</td>
            <td>80</td>
          </tr>
          <tr>
            <td>/order</td>
            <td>80</td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
            <td colSpan="2" className="stats__show-more">More...</td>
          </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
