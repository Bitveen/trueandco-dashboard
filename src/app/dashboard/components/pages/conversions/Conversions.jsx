import React from 'react';

import PageHeader from '../../common/PageHeader';
import ConversionsTotal from './ConversionsTotal';
import ConversionsBySource from './ConversionsBySource';
import ConversionsByLandingPage from './ConversionsByLandingPage';

export default class Conversions extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Conversions';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="conversions">
        <PageHeader title={this.title} />
        <ConversionsTotal />
        <ConversionsBySource />
        <ConversionsByLandingPage />
      </div>
    );
  }
}
