import React from 'react';

import PageHeader from '../../common/PageHeader';

export default class Help extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Help';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="help">
        <PageHeader title={this.title} />
      </div>
    );
  }
}
