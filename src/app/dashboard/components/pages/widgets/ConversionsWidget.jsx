import React from 'react';
import MonthlyDailyTotalWidget from './MonthlyDailyTotalWidget';

const ConversionsWidget = () => {
  return (
    <MonthlyDailyTotalWidget
      daily={{
        mobile: 13,
        desktop: 15
      }}
      monthly={{
        mobile: 20,
        desktop: 30
      }}
      title="Conversions" />
  );
};

export default ConversionsWidget;

