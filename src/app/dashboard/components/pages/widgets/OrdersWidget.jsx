import React from 'react';

const OrdersWidget = () => {
  return (
    <div className="widget">
      <div className="widget__body">
        <div className="widget__header">
          <h3 className="widget__header-text">Orders</h3>
        </div>
        <div className="widget__content">
          <div className="widget__wrapper">
            <h3 className="widget__title">Average Order Value</h3>
            <div className="widget__icons">
              <div className="widget__icon">
                <i className="fa fa-mobile" />
                <p className="widget__icon-text">54</p>
              </div>
              <div className="widget__icon">
                <i className="fa fa-laptop" />
                <p className="widget__icon-text">42</p>
              </div>
              <div className="widget__icon">
                <i className="fa fa-clone" />
                <p className="widget__icon-text">98</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrdersWidget;
