import React from 'react';
import MonthlyDailyTotalWidget from './MonthlyDailyTotalWidget';

const TrafficWidget = () => {
  return (
    <MonthlyDailyTotalWidget
      daily={{
        mobile: 13,
        desktop: 15
      }}
      monthly={{
        mobile: 20,
        desktop: 30
      }}
      title="Traffic (common)" />
  );
};

export default TrafficWidget;
