import React from 'react';

const TrafficSourcesWidget = () => {
  return (
    <div className="widget">
      <div className="widget__body">
        <div className="widget__header">
          <h3 className="widget__header-text">Traffic (by source)</h3>
        </div>
        <div className="widget__content">
          <div className="widget__wrapper">
            <table className="widget__table">
              <tbody>
              <tr>
                <td>Facebook ad:</td><td>2K</td>
              </tr>
              <tr>
                <td>Google organic:</td><td>3K</td>
              </tr>
              <tr>
                <td>Email/Sailthru:</td><td>3K</td>
              </tr>
              <tr>
                <td>Direct:</td><td>3K</td>
              </tr>
              <tr>
                <td>Google CDC:</td><td>3K</td>
              </tr>
              <tr>
                <td>Pinterest:</td><td>3K</td>
              </tr>
              <tr>
                <td>Hareasale:</td><td>3K</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TrafficSourcesWidget;
