import React from 'react';
import PropTypes from 'prop-types';

const MonthlyDailyTotalWidget = ({ title, daily, monthly }) => {
  return (
    <div className="widget">
      <div className="widget__body">
        <div className="widget__header">
          <h3 className="widget__header-text">{title}</h3>
        </div>
        <div className="widget__content">
          <div className="widget__wrapper">
            <table className="widget__table">
              <tbody>
              <tr>
                <td />
                <td className="widget__icon-cell"><i className="fa fa-mobile" /></td>
                <td className="widget__icon-cell"><i className="fa fa-laptop" /></td>
                <td className="widget__icon-cell"><i className="fa fa-clone" /></td>
              </tr>
              <tr>
                <td>Total daily</td>
                <td>{ daily.mobile }</td>
                <td>{ daily.desktop }</td>
                <td>{ daily.mobile + daily.desktop }</td>
              </tr>
              <tr>
                <td>Total monthly</td>
                <td>{ monthly.mobile }</td>
                <td>{ monthly.desktop }</td>
                <td>{ monthly.mobile + monthly.desktop }</td>
              </tr>
              </tbody>
            </table>
            <button className="widget__button">Details</button>
          </div>
        </div>
      </div>
    </div>
  );
};

MonthlyDailyTotalWidget.propTypes = {
  title: PropTypes.string.isRequired,
  daily: PropTypes.shape({
    mobile: PropTypes.number.isRequired,
    desktop: PropTypes.number.isRequired
  }).isRequired,
  monthly: PropTypes.shape({
    mobile: PropTypes.number.isRequired,
    desktop: PropTypes.number.isRequired
  })
};

export default MonthlyDailyTotalWidget;
