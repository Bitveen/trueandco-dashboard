import React from 'react';

import PageHeader from '../../common/PageHeader';
import TrafficWidget from './TrafficWidget';
import ConversionsWidget from './ConversionsWidget';
import OrdersWidget from './OrdersWidget';
import TrafficSourcesWidget from './TrafficSourcesWidget';
import AddWidget from './AddWidget';

export default class Widgets extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Set of widgets';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="widgets">
        <PageHeader title={this.title} />
        <div className="widgets-area">
          <TrafficWidget />
          <ConversionsWidget />
          <OrdersWidget />
          <TrafficSourcesWidget />
          <AddWidget />
        </div>
      </div>
    );
  }
}
