import React from 'react';

const AddWidget = () => {
  return (
    <div className="widget widget_add">
      <div className="widget__body">
        <span className="widget__plus">+</span>
        <h3 className="widget__add-title">Add new widget</h3>
      </div>
    </div>
  );
};

export default AddWidget;
