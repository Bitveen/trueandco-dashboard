import React from 'react';

import PageHeader from '../../common/PageHeader';
import AddToCartTotal from './AddToCartTotal';
import AddToCartTotalPercentage from './AddToCartTotalPercentage';

export default class AddToCart extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Add to cart';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="add-to-cart">
        <PageHeader title={this.title} />
        <AddToCartTotal />
        <AddToCartTotalPercentage />
      </div>
    );
  }
}
