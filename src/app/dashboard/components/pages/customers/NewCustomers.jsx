import React from 'react';
import moment from 'moment';
import { getCustomersSourcesPerPeriod } from '../../../../api/customers';

import StatsHeader from '../../stats/StatsHeader';
import StatsDateRange from '../../stats/StatsDateRange';
import StatsBarChart from '../../stats/StatsBarChart';

export default class NewCustomers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sources: [],
      startDate: moment(),
      endDate: moment()
    };

    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.renderChart = this.renderChart.bind(this);
  }
  componentDidMount() {
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getCustomersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      });
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
    const startDatePeriod = startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getCustomersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      });
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = endDate.format('MM/YYYY');
    getCustomersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      });
  }
  renderChart() {
    const labels = this.state.sources.map(source => source.source);
    const datasets = [
      {
        borderColor: '#f56b42',
        backgroundColor: '#f56b42',
        data: this.state.sources.map(source => source.customers)
      }
    ];
    return <StatsBarChart data={{ labels, datasets }} />;
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="New customers (by channel)" />
        <div className="stats__controls">
          <StatsDateRange
            right
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onStartDateChange={this.handleStartDateChange}
            onEndDateChange={this.handleEndDateChange} />
        </div>
        {this.renderChart()}
        {
          this.state.sources.length ? (
            <table className="stats__table">
              <thead>
              <tr>
                <th>Source</th>
                <th>Customers</th>
              </tr>
              </thead>
              <tbody>
              {
                this.state.sources.map((source, index) => {
                  return (
                    <tr key={index}>
                      <td>{source.source}</td>
                      <td>{source.customers}</td>
                    </tr>
                  );
                })
              }
              </tbody>
            </table>
          ) : (
            <p className="stats__not-found">Sources not found!</p>
          )
        }

      </div>
    );
  }
}
