import React from 'react';
import moment from 'moment';
import * as customersApi from '../../../../api/customers';

import PageHeader from '../../common/PageHeader';
import Metric from '../../common/Metric';
import CustomersTotalMonthly from './CustomersTotalMonthly';
import NewCustomers from './NewCustomers';

export default class Customers extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Customers';

    this.state = {
      totalCustomers: {
        count: 0,
        fetching: false
      },
      customersThisMonth: {
        count: 0,
        fetching: false
      }
    };
  }
  componentDidMount() {
    document.title = this.title;
    this.setState({
      totalCustomers: { fetching: true, count: 0 },
      customersThisMonth: { fetching: true, count: 0 }
    });
    customersApi
      .getTotalCustomers()
      .then((response) => {
        this.setState({
          totalCustomers: {
            count: parseInt(response.customers),
            fetching: false
          }
        });
      }, (err) => {
        this.setState({
          totalCustomers: {
            count: 0,
            fetching: false
          }
        });
      });
    customersApi
      .getTotalCustomersPerPeriod(moment().format('MM/YYYY'))
      .then((response) => {
        const newCustomers = parseInt(response.periods[0].customers.newCustomers);
        const repeatCustomers = parseInt(response.periods[0].customers.repeatCustomers);
        this.setState({
          customersThisMonth: {
            count: newCustomers + repeatCustomers,
            fetching: false
          }
        });
      }, (err) => {
        this.setState({
          customersThisMonth: {
            count: 0,
            fetching: false
          }
        });
      });

  }
  render() {
    const { totalCustomers, customersThisMonth } = this.state;
    return (
      <div className="customers">
        <PageHeader title={this.title} />
        <div className="metrics">
          <Metric
            title="Total customers"
            value={totalCustomers.count}
            color="orange" />
          <Metric
            title="Customers this month"
            value={customersThisMonth.count}
            color="yellow" />
          <Metric
            title="Gained percentage"
            value={totalCustomers.count ? `${((customersThisMonth.count / totalCustomers.count) * 100)}%` : '0%'}
            subLabel="last 30 days" />
        </div>
        <CustomersTotalMonthly />
        <NewCustomers />
      </div>
    );
  }
}
