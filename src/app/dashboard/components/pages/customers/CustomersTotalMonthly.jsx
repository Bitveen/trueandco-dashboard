import React from 'react';
import moment from 'moment';
import { getTotalCustomersPerPeriod } from '../../../../api/customers';

import StatsHeader from '../../stats/StatsHeader';
import StatsDateRange from '../../stats/StatsDateRange';
import StatsLineChart from '../../stats/StatsLineChart';

export default class CustomersTotalMonthly extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      periods: [],
      startDate: moment(),
      endDate: moment()
    };

    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
    const startDatePeriod = startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getTotalCustomersPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = endDate.format('MM/YYYY');
    getTotalCustomersPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  componentDidMount() {
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getTotalCustomersPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  renderChart() {
    const labels = this.state.periods.map(period => moment(period.period, 'DD/MM/YYYY').format('MM/YYYY'));

    const newData = this.state.periods.map(period => period.customers.newCustomers);
    const repeatData = this.state.periods.map(period => period.customers.repeatCustomers);
    const totalData = this.state.periods.map((period) => {
      return period.customers.newCustomers + period.customers.repeatCustomers;
    });

    const datasets = [
      {
        label: 'New',
        borderColor: '#a1874b',
        backgroundColor: '#a1874b',
        fill: false,
        data: newData
      },
      {
        label: 'Repeat',
        borderColor: '#f56b42',
        backgroundColor: '#f56b42',
        fill: false,
        data: repeatData
      },
      {
        label: 'Total',
        borderColor: '#c5c4c5',
        backgroundColor: '#c5c4c5',
        fill: false,
        data: totalData
      }
    ];
    return <StatsLineChart data={{ labels, datasets }} />;
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Total monthly" />
        <div className="stats__controls">
          <StatsDateRange
            right
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onStartDateChange={this.handleStartDateChange}
            onEndDateChange={this.handleEndDateChange} />
        </div>
        {this.renderChart()}
        {
          this.state.periods.length ? (
            <table className="stats__table">
              <thead>
              <tr>
                <th rowSpan="2">Period</th>
                <th colSpan="3">Customers</th>
              </tr>
              <tr>
                <th>New</th>
                <th>Repeat</th>
                <th>Total</th>
              </tr>
              </thead>
              <tbody>
              {
                this.state.periods.map((period, index) => {
                  return (
                    <tr key={index}>
                      <td>{moment(period.period, 'DD/MM/YYYY').format('MM/YYYY')}</td>
                      <td>{period.customers.newCustomers}</td>
                      <td>{period.customers.repeatCustomers}</td>
                      <td>{period.customers.newCustomers + period.customers.repeatCustomers}</td>
                    </tr>
                  );
                })
              }
              </tbody>
            </table>
          ) : (
            <p className="stats__not-found">Periods not found!</p>
          )
        }
      </div>
    );
  }
}
