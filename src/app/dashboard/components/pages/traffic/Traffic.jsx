import React from 'react';

import PageHeader from '../../common/PageHeader';
import TrafficTotal from './TrafficTotal';
import TrafficBySource from './TrafficBySource';
import TrafficByLandingPage from './TrafficByLandingPage';

export default class Traffic extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Traffic';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="traffic">
        <PageHeader title={this.title} />
        <TrafficTotal />
        <TrafficBySource />
        <TrafficByLandingPage />
      </div>
    );
  }
}
