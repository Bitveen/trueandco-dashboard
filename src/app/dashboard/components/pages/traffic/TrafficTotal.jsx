import React from 'react';
import moment from 'moment';

import StatsHeader from '../../stats/StatsHeader';
import StatsRangeTypeSelect from '../../stats/StatsRangeTypeSelect';
import StatsDateRange from '../../stats/StatsDateRange';
import StatsLineChart from '../../stats/StatsLineChart';

export default class TrafficTotal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
          label: 'Mobile',
          borderColor: '#a1874b',
          backgroundColor: '#a1874b',
          fill: false,
          data: [65, 59, 80, 81, 56, 55, 40]
        },{
          label: 'Desktop',
          borderColor: '#f56b42',
          backgroundColor: '#f56b42',
          fill: false,
          data: [23, 45, 56, 91, 21, 35, 45]
        },{
          label: 'Combined',
          borderColor: '#c5c4c5',
          backgroundColor: '#c5c4c5',
          fill: false,
          data: [67, 21, 12, 34, 45, 22, 10]
        }]
      },
      rangeType: 'monthly',
      startDate: moment(),
      endDate: moment()
    };

    this.handleRangeTypeSelect = this.handleRangeTypeSelect.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
  }
  handleRangeTypeSelect(rangeType) {
    console.log(rangeType);
    this.setState({ rangeType: rangeType.value });
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Total" />
        <div className="stats__controls">
          <StatsRangeTypeSelect
            rangeType={this.state.rangeType}
            onChange={this.handleRangeTypeSelect} />
          <StatsDateRange
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onStartDateChange={this.handleStartDateChange}
            onEndDateChange={this.handleEndDateChange} />
        </div>
        <StatsLineChart data={this.state.data} />
        <table className="stats__table">
          <thead>
          <tr>
            <th rowSpan="2">Period</th>
            <th colSpan="3">Visitors</th>
          </tr>
          <tr>
            <th>Mobile</th>
            <th>Desktop</th>
            <th>Combined</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>08/01/2017</td>
            <td>3010</td>
            <td>1200</td>
            <td>4210</td>
          </tr>
          <tr>
            <td>08/01/2017</td>
            <td>3010</td>
            <td>1200</td>
            <td>4210</td>
          </tr>
          <tr>
            <td>08/01/2017</td>
            <td>3010</td>
            <td>1200</td>
            <td>4210</td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
            <td colSpan="4" className="stats__show-more">More...</td>
          </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
