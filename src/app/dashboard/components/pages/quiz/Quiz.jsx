import React from 'react';

import PageHeader from '../../common/PageHeader';

export default class Quiz extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Quiz';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="quiz">
        <PageHeader title={this.title} />
      </div>
    );
  }
}
