import React from 'react';
import moment from 'moment';

import StatsDateRange from '../../stats/StatsDateRange';
import StatsLineChart from '../../stats/StatsLineChart';

export default class NewRelicTotal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
          label: 'AVG resp time',
          borderColor: '#a1874b',
          backgroundColor: '#a1874b',
          fill: false,
          data: [65, 59, 80, 81, 56, 55, 40]
        },{
          label: 'AVG page load, mobile',
          borderColor: '#f56b42',
          backgroundColor: '#f56b42',
          fill: false,
          data: [23, 45, 56, 91, 21, 35, 45]
        },{
          label: 'AVG page load, desktop',
          borderColor: '#c5c4c5',
          backgroundColor: '#c5c4c5',
          fill: false,
          data: [67, 21, 12, 34, 45, 22, 10]
        }]
      },
      startDate: moment(),
      endDate: moment()
    };

    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
  }
  render() {
    return (
      <div className="stats">
        <div className="stats__controls">
          <StatsDateRange
            right
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onStartDateChange={this.handleStartDateChange}
            onEndDateChange={this.handleEndDateChange} />
        </div>
        <StatsLineChart data={this.state.data} />
        <table className="stats__table">
          <thead>
          <tr>
            <th rowSpan="2">Period</th>
            <th colSpan="3">Metric</th>
          </tr>
          <tr>
            <th>AVG Response time</th>
            <th>AVG Page load, mobile</th>
            <th>AVG Page load, desktop</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>08/01/2017</td>
            <td>100</td>
            <td>30</td>
            <td>130</td>
          </tr>
          <tr>
            <td>08/01/2017</td>
            <td>100</td>
            <td>30</td>
            <td>130</td>
          </tr>
          <tr>
            <td>08/01/2017</td>
            <td>100</td>
            <td>30</td>
            <td>130</td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
            <td colSpan="4" className="stats__show-more">More...</td>
          </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
