import React from 'react';

import PageHeader from '../../common/PageHeader';
import Metric from '../../common/Metric';
import NewRelicTotal from './NewRelicTotal';

export default class NewRelic extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'New Relic metrics';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="new-relic">
        <PageHeader title={this.title} />
        <div className="metrics">
          <Metric title="Average response time" value="1.1 s" subLabel="today" color="orange" />
          <Metric title="Average page load speed, mobile" value="1.3 s" subLabel="today" color="yellow" />
          <Metric title="Average page load speed, desktop" value="0.95 s" subLabel="today" />
        </div>
        <NewRelicTotal />
      </div>
    );
  }
}
