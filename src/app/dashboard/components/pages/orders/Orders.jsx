import React from 'react';

import PageHeader from '../../common/PageHeader';
import OrdersTotal from './OrdersTotal';
import OrdersBySource from './OrdersBySource';
import OrdersAverageValue from './OrdersAverageValue';

export default class Orders extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Orders';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="orders">
        <PageHeader title={this.title} />
        <OrdersTotal />
        <OrdersBySource />
        <OrdersAverageValue />
      </div>
    );
  }
}
