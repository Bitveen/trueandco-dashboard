import React from 'react';
import moment from 'moment';
import { getOrdersSourcesPerPeriod } from '../../../../api/orders';

import StatsHeader from '../../stats/StatsHeader';
import StatsDateRange from '../../stats/StatsDateRange';
import StatsBarChart from '../../stats/StatsBarChart';

export default class OrdersBySource extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sources: [],
      startDate: moment(),
      endDate: moment()
    };

    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
  }
  componentDidMount() {
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getOrdersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      })
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
    const startDatePeriod = startDate.format('MM/YYYY');
    const endDatePeriod = this.state.endDate.format('MM/YYYY');
    getOrdersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      })
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
    const startDatePeriod = this.state.startDate.format('MM/YYYY');
    const endDatePeriod = endDate.format('MM/YYYY');
    getOrdersSourcesPerPeriod(startDatePeriod, endDatePeriod)
      .then((response) => {
        this.setState({ sources: response.sources });
      })
  }
  renderChart() {
    const labels = this.state.sources.map(source => source.source);
    const datasets = [
      {
        borderColor: '#f56b42',
        backgroundColor: '#f56b42',
        data: this.state.sources.map(source => source.orders)
      }
    ];
    return <StatsBarChart data={{ labels, datasets }} />
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Orders by source" />
        <div className="stats__controls">
          <StatsDateRange
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onEndDateChange={this.handleEndDateChange}
            onStartDateChange={this.handleStartDateChange}
            right />
        </div>
        {this.renderChart()}
        {
          this.state.sources.length ? (
            <table className="stats__table">
              <thead>
              <tr>
                <th>Source</th>
                <th>Orders</th>
              </tr>
              </thead>
              <tbody>
              {
                this.state.sources.map((source, index) => {
                  return (
                    <tr key={index}>
                      <td>{source.source}</td>
                      <td>{source.orders}</td>
                    </tr>
                  );
                })
              }
              </tbody>
            </table>
          ) : (
            <p className="stats__not-found">Sources not found!</p>
          )
        }
      </div>
    );
  }
}
