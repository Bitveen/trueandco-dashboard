import React from 'react';
import moment from 'moment';
import { getAverageOrderValuesPerPeriod } from '../../../../api/orders';

import StatsHeader from '../../stats/StatsHeader';
import StatsRangeTypeSelect from '../../stats/StatsRangeTypeSelect';
import StatsDateRange from '../../stats/StatsDateRange';
import StatsLineChart from '../../stats/StatsLineChart';

export default class OrdersAverageValue extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      periods: [],
      rangeType: 'monthly',
      startDate: moment(),
      endDate: moment()
    };

    this.handleRangeTypeSelect = this.handleRangeTypeSelect.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
  }
  componentDidMount() {
    const startDatePeriod = this.state.startDate.format('DD/MM/YYYY');
    const endDatePeriod = this.state.endDate.format('DD/MM/YYYY');
    getAverageOrderValuesPerPeriod(startDatePeriod, endDatePeriod, this.state.rangeType)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  handleRangeTypeSelect(rangeType) {
    this.setState({ rangeType: rangeType.value });
    const startDatePeriod = this.state.startDate.format('DD/MM/YYYY');
    const endDatePeriod = this.state.endDate.format('DD/MM/YYYY');
    getAverageOrderValuesPerPeriod(startDatePeriod, endDatePeriod, rangeType.value)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  handleStartDateChange(startDate) {
    this.setState({ startDate });
    const startDatePeriod = startDate.format('DD/MM/YYYY');
    const endDatePeriod = this.state.endDate.format('DD/MM/YYYY');
    getAverageOrderValuesPerPeriod(startDatePeriod, endDatePeriod, this.state.rangeType)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  handleEndDateChange(endDate) {
    this.setState({ endDate });
    const startDatePeriod = this.state.startDate.format('DD/MM/YYYY');
    const endDatePeriod = endDate.format('DD/MM/YYYY');
    getAverageOrderValuesPerPeriod(startDatePeriod, endDatePeriod, this.state.rangeType)
      .then((response) => {
        this.setState({ periods: response.periods });
      });
  }
  renderChart() {
    const labels = this.state.periods.map(period => moment(period.period, 'DD/MM/YYYY').format('MM/YYYY'));

    const mobileData = this.state.periods.map(period => period.orders.mobile);
    const desktopData = this.state.periods.map(period => period.orders.desktop);
    const combinedData = this.state.periods.map((period) => {
      return period.orders.mobile + period.orders.desktop;
    });

    const datasets = [
      {
        label: 'Mobile',
        borderColor: '#a1874b',
        backgroundColor: '#a1874b',
        fill: false,
        data: mobileData
      },
      {
        label: 'Desktop',
        borderColor: '#f56b42',
        backgroundColor: '#f56b42',
        fill: false,
        data: desktopData
      },
      {
        label: 'Combined',
        borderColor: '#c5c4c5',
        backgroundColor: '#c5c4c5',
        fill: false,
        data: combinedData
      }
    ];
    return <StatsLineChart data={{ labels, datasets }} />;
  }
  render() {
    return (
      <div className="stats">
        <StatsHeader title="Average order value" />
        <div className="stats__controls">
          <StatsRangeTypeSelect
            rangeType={this.state.rangeType}
            onChange={this.handleRangeTypeSelect} />
          <StatsDateRange
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onStartDateChange={this.handleStartDateChange}
            onEndDateChange={this.handleEndDateChange} />
        </div>
        {this.renderChart()}
        {
          this.state.periods.length ? (
            <table className="stats__table">
              <thead>
              <tr>
                <th rowSpan="2">Period</th>
                <th colSpan="3">Order value</th>
              </tr>
              <tr>
                <th>Mobile</th>
                <th>Desktop</th>
                <th>Combined</th>
              </tr>
              </thead>
              <tbody>
              {
                this.state.periods.map((period, index) => {
                  return (
                    <tr key={index}>
                      <td>{moment(period.period, 'DD/MM/YYYY').format('MM/YYYY')}</td>
                      <td>{period.orders.mobile}$</td>
                      <td>{period.orders.desktop}$</td>
                      <td>{period.orders.mobile + period.orders.desktop}$</td>
                    </tr>
                  );
                })
              }
              </tbody>
            </table>
          ) : (
            <p className="stats__not-found">Periods not found!</p>
          )
        }
      </div>
    );
  }
}
