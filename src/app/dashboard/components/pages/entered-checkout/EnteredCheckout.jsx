import React from 'react';

import PageHeader from '../../common/PageHeader';
import EnteredCheckoutTotal from './EnteredCheckoutTotal';
import EnteredCheckoutPercentage from './EnteredCheckoutPercentage';

export default class EnteredCheckout extends React.Component {
  constructor(props) {
    super(props);

    this.title = 'Entered checkout';
  }
  componentDidMount() {
    document.title = this.title;
  }
  render() {
    return (
      <div className="entered-checkout">
        <PageHeader title={this.title} />
        <EnteredCheckoutTotal />
        <EnteredCheckoutPercentage />
      </div>
    );
  }
}
