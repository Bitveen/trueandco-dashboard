import React from 'react';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {
  return (
    <section className="sidebar hidden-xs hidden-sm">
      <div className="top-bar visible-xs visible-sm">
        <button className="menu-button" id="close-mobile-menu">
          <i className="fa fa-times" aria-hidden="true" />
        </button>
        <div className="logo logo_mobile">
          <NavLink to="/" className="logo__img" />
        </div>
      </div>
      <div className="logo visible-md visible-lg">
        <NavLink to="/" className="logo__img" />
      </div>
      <nav className="menu">
        <h4 className="menu__title">Google Analytics</h4>
        <ul className="menu__list">
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/traffic">Traffic</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/conversions">Conversions</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/orders">Orders</NavLink>
          </li>
        </ul>
        <h4 className="menu__title">Internal Metrics</h4>
        <ul className="menu__list">
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/customers">Customers</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/quiz">Quiz</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/add-to-cart">Add to cart</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/entered-checkout">Entered checkout</NavLink>
          </li>
        </ul>
        <h4 className="menu__title">External Metrics</h4>
        <ul className="menu__list">
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/new-relic">New relic</NavLink>
          </li>
        </ul>
        <div className="menu__separator">
          <div className="menu__separator-line" />
        </div>
        <ul className="menu__list">
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/settings">Settings</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="menu__link_active" className="menu__link" to="/help">Help</NavLink>
          </li>
        </ul>
      </nav>
    </section>
  );
};

export default Sidebar;
