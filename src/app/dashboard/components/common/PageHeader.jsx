import React from 'react';
import PropTypes from 'prop-types';

const PageHeader = ({ title }) => {
  return (
    <div className="page-header">
      <h3 className="page-header__text">{title}</h3>
    </div>
  );
};

PageHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default PageHeader;
