import React from 'react';
import PropTypes from 'prop-types';

const Metric = ({ title, value, subLabel, color }) => {
  const additionalClass = color ? `metrics__label_${color}` : '';
  return (
    <div className="metrics__metric">
      <h3 className="metrics__title">{title}</h3>
      <div className={`metrics__label ${additionalClass}`}>{value}</div>
      {subLabel && <p className="metrics__sub-label">{subLabel}</p>}
    </div>
  );
};

Metric.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  subLabel: PropTypes.string,
  color: PropTypes.oneOf(['yellow', 'orange'])
};

export default Metric;
