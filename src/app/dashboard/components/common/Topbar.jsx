import React from 'react';
import PropTypes from 'prop-types';

const Topbar = ({ onLogout, authenticated }) => {
  return (
    <div className="top-bar">
      <div className="visible-xs visible-sm">
        <button className="menu-button" id="show-mobile-menu">
          <i className="fa fa-bars" />
        </button>
        <div className="logo logo_mobile">
          <a href="/" className="logo__img" />
        </div>
      </div>
      {
        authenticated && (
          <div className="top-bar__logout">
            <button className="top-bar__logout-button" onClick={onLogout}>
              <i className="fa fa-sign-out"/>
              <span>Log out</span>
            </button>
          </div>
        )
      }
    </div>
  );
};

Topbar.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  onLogout: PropTypes.func.isRequired
};

export default Topbar;
