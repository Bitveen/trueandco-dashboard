import React from 'react';

const defaultOptions = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};

const defaultLegend = {
  position: 'bottom'
};

const chartWithDefaults = (Component) => {
  return ({ data }) => {
    return <Component data={data} legend={defaultLegend} options={defaultOptions} width={400} height={400} />
  };
};

export default chartWithDefaults;
