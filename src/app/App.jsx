import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';

import Login from './login/Login';
import Dashboard from './dashboard/Dashboard';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      authenticated: true,
      user: {
        email: ''
      }
    };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  handleLogin({ email, password }) {
    this.setState({
      authenticated: true,
      user: { email }
    });
    this.props.history.push('/');
  }
  handleLogout() {
    this.setState({
      authenticated: false,
      user: { email: '' }
    });
  }
  render() {
    return (
      <main className="container-fluid">
        {
          this.state.authenticated ? (
            <Dashboard
              authenticated={this.state.authenticated}
              onLogout={this.handleLogout}
              user={this.state.user} />
          ) : (
            this.props.location.pathname !== '/login' && <Redirect to="/login" />
          )
        }
        <Route path="/login" exact render={() =>
          <Login
            authenticated={this.state.authenticated}
            onLogin={this.handleLogin} />
        } />
      </main>
    );
  }
}

export default withRouter(App);
